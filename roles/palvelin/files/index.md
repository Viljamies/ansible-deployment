---
title: Otaniemen lukion fablab
---

::: Navbar

![Otakarhu](Otakarhu.png)

# OTANIEMEN LUKION FABLAB

- [Tervetuloa](#tervetuloa-fablabiin)
- [3d-Mallintaminen](#d-mallintaminen)
- [Yhteystiedot](#yhteystiedot)

:::

::: Content
## Tervetuloa FabLabiin

### Telegram

[Liity nyt FabLabin telegram ryhmään!](https://t.me/joinchat/WYaf48cddcMzZjU8)

### T̶e̶l̶e̶g̶r̶a̶m̶  Gitlab

[Liity nyt FabLabin t̶e̶l̶e̶g̶r̶a̶m̶  gitlab ryhmään (öpen söyrse köde)!](https://gitlab.com/otafablab)

### Säännöt

1. Vie kaikki roskat roskiin
2. Älä käytä useaa tulostinta samaan aikaan
3. Keskeytä tulostus joka epäonnistuu

### Portaali

[Avaa Lepää rauhassa (RiP) portaali](/portal)

Portaalin salasanan saat [ylläpidosta](#ylläpidon-yhteystiedot)

## 3D-Mallintaminen

### Configurations

- [Ultimaker 2+](https://gitlab.com/otafablab/prusaslicer-config/-/raw/main/ultimaker2+.ini?inline=false)
- [Prenta Duo XL](https://gitlab.com/otafablab/prusaslicer-config/-/raw/main/prenta_duo_xl.ini?inline=false)

### 3D-mallintaminen ja suuunnittelu - lukiokurssi

- [Osa 1](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-1/)
- [Osa 2](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-2/)
- [Osa 3](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-3/)
- [Osa 4](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-4/)
- [Osa 5](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-5/)

## Yhteystiedot

### Ylläpidon yhteystiedot

- Niklas Halonen, Fablab toimitusjohtaja
  - <https://t.me/niklashh>
- Matias Zwinger, Fablab varapuheenjohtaja ja laboratoriomanageri
  - <https://t.me/matias_z>
- Matti Heikkinen, Matematiikan ja fysiikan opettaja
  - <https://t.me/Heikkinen>
- Severi Lybeck, FabLab-Verkkosivun teema
:::
